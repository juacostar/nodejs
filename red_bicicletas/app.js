require('dotenv').config();
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const passport = require('./config/passport');
const session = require('express-session');
const jwt = require('jsonwebtoken');
const MongoDBStore = require('connect-mongodb-session')(session);
const newRelic = require('newrelic');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var bicicletasRouter = require('./routes/bicicletas');
var bicicletasAPIRouter = require('./routes/api/bicicletas');
var usuariosAPIRouter = require('./routes/api/usuarios');
var tokenRouter = require('./routes/token');
var usuariosRouter = require('./routes/usuarios');
var authAPIRouter = require('./routes/api/auth');

const Usuario = require('./models/usuario');
const Token = require('./models/token');

let store;
if(process.env.NODE_ENV === 'development'){
    store = new session.MemoryStore;
}else{
    store = new MongoDBStore({
        uri: process.env.MONGO_URI,
        collection: 'sessions'
    });
    store.on('error', function(error){
        assert.ifError(error);
        assert.ok(false);
    });
}

var app = express();

app.set('secretKey', 'jwt_pwd_!!223344'); // semilla del cifrado
app.use(session({
    cookie: {maxAge: 240 * 60 * 60  + 1000}, // 1 dia duración cookie
    store: store,
    saveUninitialized:true,
    resave: 'true',
    secret: 'red_bicis_!!!***!"*!"*!"*!"123123' // semilla codigo de identificación
}));

var mongoose = require('mongoose'); // mongoose par realizar la conexión
const usuario = require('./models/usuario');
const { assert } = require('console');
// var mongoDB = 'mongodb://localhost/red_bicicletas'; // link para conexión a db, usar en ambiente de desarrollo
var mongoDB = process.env.MONGO_URI; // usar en ambinte de producción
mongoose.connect(mongoDB,{useNewUrlParser: true});
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error: ')); // subscribirnos al evento

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// inicializar passport despues de cookie parser
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/login', function(req, res){
    res.render('session/login');
});

app.post('/login', function(req, res, next){
    passport.authenticate('local', function(err,user, info){
        if (err) return next(err);
        if (!usuario) return res.render('session/login', {info});
        req.logIn(usuario, function(err){
            if(err) return next(err);
            return res.redirect('/');
        });
    })(req, res, next);
});

app.get('/logout', function(req, res){
    req.logOut();
    res.redirect('/');
});

app.get('/forgtoPassword', function(req, res){
});

app.post('/forgotPassword', function(req, res){
});

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/usuarios', usuariosRouter);
app.use('/token', tokenRouter);
app.use('/bicicletas', loggedIn,bicicletasRouter); // middelware, se ejecuta primero el loggedIn, segurizar ruta, SOLO PARTE WEB NO PARTE API
app.use('/api/bicicletas', validarUsuario,bicicletasAPIRouter); // usualmente se ponen versiones
app.use('/api/usuarios', usuariosAPIRouter); // usualmente se ponen versiones
app.use('api/auth', authAPIRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

app.get('/auth/google',
  passport.authenticate('google', { scope: ['profile'] }));

app.get('/auth/google/callback', 
  passport.authenticate('google', { 
      failureRedirect: '/error',
      successRedirect: '/'
    }),
  function(req, res) {
    // Successful authentication, redirect home.
    res.redirect('/');
  });

function loggedIn(req, res, next){
    if(req.user){
        next();
    }else{
        console.log('user sin loguearse');
        res.redirect('login');
    }

}

function validarUsuario(req, res, next){
    jwt.verify(req.headers['x-acces-token'], req.app.get('secretKey'), function(err, decoded){
        if(err){
            res.json({status: 'error', message: err.message, data:null});
        }else{
            req.body.userId = decoded.id;
            console.log('jwt verify' + decoded);
            next();
        }  
    });

}

module.exports = app;