var moongose = require('mongoose');
var Schema = moongose.Schema;

var bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: {type: '2dsphere', sparse: true}
    }
}); // crear el schema para mongo

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion){
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    }
    );
};

bicicletaSchema.methods.toString = function(){
    return 'id: ' + this.id + ' | color: ' + this.color;
};

bicicletaSchema.statics.allBicis = function(cb){
    return this.find({}, cb);
};

bicicletaSchema.statics.add = function(aBici, cb){
    this.create(aBici, cb);
};

bicicletaSchema.statics.findByCode = function(aCode, cb){
    return this.findOne({code: aCode}, cb);
};

bicicletaSchema.statics.removeByCode = function(aCode, cb){ // cb hace de callback
    return this.delteOne({code: aCode}, cb);
};
// methods: metodos de instancia, statics: directo del modelo

module.exports = moongose.model('Bicicleta', bicicletaSchema); // exportar el modelo para mongo


/* var Bicicleta = function(id, color, modelo, ubicacion) { // funcion que hace de constructor
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}


Bicicleta.allBicis = []; // crea el nuevo atributo
Bicicleta.add = function(aBici) { // crea nuevo metodopara clase Bicileta
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(aBiciId) {
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if (aBici)
        return aBici;
    else
        throw new Error('No existe bicicleta con ese id');

}

Bicicleta.removeBiId = function(aBiciId) {
    for (var i = 0; i < Bicicleta.allBicis.length; i++) {
        if (Bicicleta.allBicis[i].id == aBiciId) {
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
}

/* var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424, -58.3861497]);
var b = new Bicicleta(2, 'blanca', 'urbana', [-34.596932, -58.380287]); 

Bicicleta.add(a);
Bicicleta.add(b);*/

// module.exports = Bicicleta; //  para poder exportarlo
