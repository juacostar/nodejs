var moongose = require('mongoose');
var Bicicleta = require('../../../models/bicicleta');

describe('Testing bicicletas', function(){
    beforeEach(function(){ // antes de setup de db
        var mongoDB = 'mongodb://localhost/testdb';
        moongose.connect(mongoDB, { useNewUrlParser: true});
        const db = moongose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database: ');
            
        });
    });

    afterEach(function(done){ // despues de, borrar todos los datos de los test, el done es para el asincronismo
        Bicicleta.deleteMany({}, function(err, succes){
            if (err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstane', () =>{
        it('crea una instancia de bicicleta', () =>{
            var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.1);
        })
    });

    describe('Bicicleta.allBicis',() => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done(); 
            });
        });
    });

    describe('Bicicleta.add', () =>{
        it('agrega solo una bici', (done) => {
            var aBici = new Bicicleta({code:1, color:"verde", modelo: "urbana"});
            Bicicleta.add(aBici, function(err, newBici){
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done(); 
                })
            })
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('debe volver la bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                var aBici = new Bicicleta({code: 1, color: "verde", modelo:"urbana"});
                Bicicleta.add(aBici, function(err, newBici){
                    if (err) console.log(err);
                    var aBici2 = new Bicicleta({code:2, color:"roja", modelo:"urbana"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if (err) console.log(err);
                        Bicicleta.findByCode(1, function(err, targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);

                            done();
                        });
                    });
                });
            });
        });
    });

    
});

/*
beforeEach(() => {
    Bicicleta.allBicis = [];
}) // metodo que se ejecuta antes de ejecutar los test

describe('Bicicleta.allBicis',() => {
    it('comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicileta.add', () =>{
    it('agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424, -58.3861497]);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.findById', () => {
    it('debe devolver la bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var abici = new Bicicleta(1, 'verde', 'urbana');
        var abici2 = new Bicicleta(2, 'verde', 'urbana');
        Bicicleta.add(abici);
        Bicicleta.add(abici2);
        var taregtBici = Bicicleta.findById(1);
        expect(taregtBici.id).toBe(1);
        expect(taregtBici.color).toBe(abici.color);
        expect(taregtBici.modelo).toBe(abici.modelo);
    });
});
*/