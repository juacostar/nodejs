// instalacion de request para las peticiones
var moongose = require('mongoose');
var Bicicleta = require('../../../models/bicicleta');
var request = require('request');
var server = require('../../../bin/www'); // no funciona el require
const { base } = require('../../../models/bicicleta');

var base_url = "http://localhost:5000/api/bicicletas";

describe('Bicicleta API', () => {
    beforeEach(function(){ // antes de setup de db
        var mongoDB = 'mongodb://localhost/testdb';
        moongose.connect(mongoDB, { useNewUrlParser: true});
        const db = moongose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database: ');
            
        });
    });

    afterEach(function(done){ // despues de, borrar todos los datos de los test, el done es para el asincronismo
        Bicicleta.deleteMany({}, function(err, succes){
            if (err) console.log(err);
            done();
        });
    });

    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            request.get(base_url, function(error, response, body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });
    // es el json no funciona
    describe('POST BICICLETAS /create', () => {
        it('status 200', (done) => {
            var headers2 = {'content-type' : 'application/json'};
            var aBici = '{"code": 10, "color": "rojo", "modelo": "urbana", "lat":-34, "lng": -54 }';
            request.post({
                headers: headers2,   
                url: base_url + '/create',
                body: aBici
            }, function(error, response, body){ 
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe("rojo");
                expect(bici.ubicacion[0]).toBe(-34);
                expect(bici.ubicacion[1]).toBe(-54);
                done();
            });
        });
    });
});

